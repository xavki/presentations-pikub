%title: PiKub
%author: xavki

# PiKub : installation de l'OS Raspbian

<br>
## Télécharger une ISO

https://www.raspberrypi.org/downloads/raspbian/
Remarque : pour kubernetes il vaut mieux de la stretch (version docker)

https://raspberry-pi.fr/telechargements/

<br>
## Télécharger un installateur d'ISO

* pas obligatoire : passage par la commande dd

* https://www.balena.io/etcher/

* unzip balena-etcher-electron-1.5.52-linux-x64.zip

* chmod 755 balenaEtcher-1.5.52-x64.AppImage

* lancement : ./balenaEtcher-1.5.52-x64.AppImage

<br>
## Installation de l'image

* choisir l'image et la carte sd

* ajout au Raspberry puis lancement sur un écran avec clavier


--------------------------------------------------------------------

# PiKub : ssh

<br>
* service ssh

```
systemctl enable ssh
service ssh start
```

* édition du fichier dhcpd

```
vim /etc/network/interfaces


auto eth0
iface eth0 inet static
address 192.168.1.20
netmask 255.255.255.0
gateway <ip_box>
```

* changement du hostname

```
vim /etc/hostname
```

* reboot ou network uniquement

```   
sudo /etc/init.d/networking restart
```

---------------------------------------------------------------------

# Sans écran


* pour activer ssh 

ajouter un fichier ssh vide à la racine de /boot


* pour ip statique

compléter le fichier /rootfs/etc/dhcpcd.conf


```
interface eth0
static ip_address=192.168.1.20
static routers=192.168.1.254
static domain_name_servers=192.168.1.254
```
