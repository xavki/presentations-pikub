%title: PiKub
%author: xavki

# PiKub : matériel


<br>
MATERIEL :

* 6 raspberry pi >= modele 3
	-	35 euros x 6 = 210 €

* 6 dissipateurs
	- 6 euros x 2 = 12 €

* 1 plaque cluster 
	-	20 €

* 1 transformateur
	- 36 €

* 6 carte SD
	- 30 euros x 6 = 180 €

* 1 swith 8 ports
	- 28 €

* 6 cables RJ45
	- 14 €

* 6 cable usb C
	- 10 euros x 2 = 20 €

TOTAL : 520 euros
Rq : dépendance coût des cartes SD (64G)

-----------------------------------------------------------------------

# PiKub : Remerciements et dons

<br>

PAYPAL : 65 euros

LEETCHI : 155 euros

Total : 220 euros

<br>
DONS :

- 3 raspberry 3 Model B + 3 carte SD = 195 €

- 3 raspberry 4 4G = 180 €

Donc en plus achat carte SD 256G + sondes Pi

<br>
Merci ! : 
Steve + Lorena + Yoan + Guillaume + Simon + Lucas + Michael + Thomas
Famille M + Armand + Sivaguru + Mikaël + Nawfal
David + Muriel + Youssef


------------------------------------------------------------------------


# PiKub : étapes


1. Montage


2. Installation des cartes SD : système d'exploitation 


3. Installation de docker


4. Installation de kubernetes


5. Premiers tests
